FROM apachepulsar/pulsar-manager:v0.3.0

RUN apt-get update && \
    apt-get install -y netcat

COPY files/entrypoint.sh /pulsar-manager
COPY files/startup_remote.sh /pulsar-manager
# COPY files/init_db.sql /pulsar-manager

RUN chmod 700 /pulsar-manager/entrypoint.sh && \
    chmod 700 /pulsar-manager/startup_remote.sh
