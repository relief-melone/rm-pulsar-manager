export PGPASSWORD=$PASSWORD

nc -z -w15 $POSTGRESQL_HOST $POSTGRESQL_PORT

if [ $? = "0" ]; then
  echo "Server available. Initializing Database..."
  psql \
  -h $POSTGRESQL_HOST \
  -p $POSTGRESQL_PORT \
  -d $POSTGRESQL_DB \
  -U $USERNAME \
  -f /pulsar-manager/init_db.sql
else
 echo "Could not connect to database ${POSTGRESQL_HOST}:${POSTGRESQL_PORT}. Exiting..."
 exit 1
fi

